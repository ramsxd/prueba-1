package com.example.duoc.prueba;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegistroActivity extends AppCompatActivity {

    private EditText etUsuario,etClave,etRepetir;
    private Button btnCrear, btnVolver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        etUsuario = (EditText) findViewById(R.id.etUsuario);
        etClave = (EditText) findViewById(R.id.etClave);
        etRepetir=(EditText)findViewById(R.id.etRclave) ;
        btnCrear = (Button) findViewById(R.id.btnCrear);
        btnVolver = (Button) findViewById(R.id.btnVolver);

        btnVolver.setOnClickListener(new View.OnClickListener()

                                      {
                                          @Override
                                          public void onClick(View v) {
                                             volver();
                                          }




                                      }

        );
        btnCrear.setOnClickListener(new View.OnClickListener() {

                                          @Override
                                          public void onClick(View v) {
                                              guardarFormulario();
                                          }
                                      }

        );


    }

    private void guardarFormulario() {

        if (etUsuario.getText().toString().length() >0 && etClave.getText().toString().equals(etRepetir.getText().toString())) {
            Usuario nuevoForm = new Usuario();
            nuevoForm.setUsuario(etUsuario.getText().toString());
            nuevoForm.setClave(etClave.getText().toString());

            FormularioRegistro.agregarFormulario(nuevoForm);

            Toast.makeText(this, "Guardado", Toast.LENGTH_SHORT).show();

        }else{
            Toast.makeText(this, "No guardo", Toast.LENGTH_SHORT).show();
        }
    }
    private void volver() {
        Intent i = new Intent(RegistroActivity.this,PruebaActivity.class);
        startActivity(i);
    }
    }

