package com.example.duoc.prueba;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PruebaActivity extends AppCompatActivity {
    private EditText etUsuario;
    private EditText etClave;
    private Button btnEntrar;
    private  Button btnRegistrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prueba);

        etUsuario = (EditText) findViewById(R.id.etUsuario);
        etClave = (EditText) findViewById(R.id.etClave);
        btnEntrar=(Button)findViewById(R.id.btnEntrar);
        btnRegistrar=(Button)findViewById(R.id.btnRegistrar);

        btnEntrar.setOnClickListener(new View.OnClickListener()

                                        {
                                            @Override
                                            public void onClick(View v) {
                                                Entrar();
                                            }




                                        }

        );
        btnRegistrar.setOnClickListener(new View.OnClickListener()

                                     {
                                         @Override
                                         public void onClick(View v) {
                                             Registrar();
                                         }




                                     }

        );

    }

    private void Entrar() {
FormularioRegistro f = new FormularioRegistro();


       if(f.getFormularios().isEmpty()){
           Toast.makeText(this, "Error no se pudo ingresar", Toast.LENGTH_SHORT).show();
       }
        else
       {
           Toast.makeText(this, "Ingresado", Toast.LENGTH_SHORT).show();

       }

    }





    private void Registrar() {
        Intent i = new Intent(PruebaActivity.this,RegistroActivity.class);
        startActivity(i);
    }
}
